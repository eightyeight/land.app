<?php

/**
 * Application component of .
 * *
 * @author 
 * @package 
 * @since 
 */
class Amocrm extends CApplicationComponent
{

	/**
	 * @var string Наш аккаунт - поддомен amOCRM
	 */
	private $AMOCRM_SUBDOMAIN = "giraffe";

	/**
	 * @var string Наш аккаунт - логин пользователя amoCRM
	 */
	private $AMOCRM_MASTER_LOGIN = 'bodrov@gf-a.ru';

	/**
	 * @var string Наш аккаунт - Хэш-ключ пользователя amoCRM
	 */
	private $AMOCRM_MASTER_HASH = 'd2bdeaafafd39e56c06d5b68bb3f19a5';

	/**
	 * @var string Наш аккаунт - Срок выполнения новой задачи
	 */
	private $task_complete_till = 10800; //3 часа. 

	/**
	 * @var string Наш аккаунт - ID тветственного по умолчанию менеджер
	 */
	private $default_responsible_user_id = 207392;

	/**
	 * @var array ID полей в amoCRM
	 */
	private $custom_fields = array(
		'phone' => 454558,
		'email' => 454560,
	);

	/**
	 * Функция добавления данных формвы в amoCRM
	 * @param Form Экземпляр класса Form, содержащий данные формы.
	 * @return boolean успех добавления в amoCRM
	 */
	public function addFormData($data)
	{
		$data = (array)$data;
		$timestart = time();

		if (isset($data['phone'])) {
			$data['phone'] = preg_replace('/[^\d+]/', '', $data['phone']);
		}

		//подключение файлов
		require_once(dirname(__FILE__) . '/common2.php.inc');
		require_once(dirname(__FILE__) . '/logger.php.inc');

		//инициализация логгера
		init_logger('log', true, false, dirname(__FILE__).'/log');
		log_event();
		log_event('Start at '.$timestart);
		log_event(json_encode($data), 'DEBUG');

		//авторизация
		try {
			amo_curl_authorize(array('USER_LOGIN' => $this->AMOCRM_MASTER_LOGIN,
						   'USER_HASH' => $this->AMOCRM_MASTER_HASH), $this->AMOCRM_SUBDOMAIN);
		}
		catch(Exception $E)	{
			log_event('Auth error', 'ERROR');
			return false;
		}

		//создание сделки
		try {
			$postData['request']['leads']['add'][] = array(
				'name' => "Новая сделка. ". date('d.m.Y H:i:s', $timestart),
				'tags' => 'print.gf-a.ru',
			);

			log_event(json_encode($postData), 'DEBUG');
			$responce = amo_curl_post('/private/api/v2/json/leads/set', $postData, $this->AMOCRM_SUBDOMAIN);

			if(!isset($responce['leads']['add'][0]['id'])) {
				log_event('Failed to create lead. No id.', 'ERROR');
				return false;
			}
			$lead_id = $responce['leads']['add'][0]['id'];
			log_event("Lead was created. ID: $lead_id");
			unset($postData);
		}
		catch(Exception $E)	{
			log_event('Failed to create lead', 'ERROR');
			log_event('Error '.$E->getCode().': '.$E->getMessage(), 'ERROR');
			return false;
		}

		//проверка существования контакта
		try {
			$contact = $this->amocrm_check(isset($data['phone'])? $data['phone']: null, isset($data['email'])? $data['email']: null);
		}
		catch(Exception $E)	{
			log_event('Amocrm_check error', 'ERROR');
			return false;
		}

		//создание контакта
		if(empty($contact)) {
			log_event("Contact not found. Creating");
			try {
				$postData['request']['contacts']['add'][] = array(
					'name' => !empty($data['name'])? $data['name'] : "Новый контакт",
					'tags' => 'print.gf-a.ru',
					'custom_fields'=>array(),
					'linked_leads_id'=>array($lead_id),
					'responsible_user_id' => $this->default_responsible_user_id,
				);

				if(!empty($data['phone'])) {
					$postData['request']['contacts']['add'][0]['custom_fields'][] = array(
						'id' => $this->custom_fields['phone'],
						'values' => array(
							array(
								'value' => $data['phone'],
                                'enum'=>'WORK'
							),
						),
					);
				}

				if(!empty($data['email'])) {


					$postData['request']['contacts']['add'][0]['custom_fields'][] = array(
						'id' => $this->custom_fields['email'],
						'values' => array(
							array(
								'value' => $data['email'],
                                'enum' => 'WORK'
							),
						),
					);
				}

                ob_start();
                    echo json_encode($postData);
                    $dumpdump = ob_get_contents();
                ob_end_clean();
                log_event('Dump of post query:' . $dumpdump);
				log_event(json_encode($postData), 'DEBUG');
				amo_curl_post('/private/api/v2/json/contacts/set', $postData, $this->AMOCRM_SUBDOMAIN);

//				if(!isset($responce['contacts']['add'][0]['id'])) {
//					log_event('Failed to create contact. No id.', 'ERROR');
//					return false;
//				}
				log_event("Contact was created. ID: ". $responce['contacts']['add'][0]['id']);

				unset($postData);
			}
			catch(Exception $E)	{
				log_event('Failed to create contact', 'ERROR');
				log_event('Error '.$E->getCode().': '.$E->getMessage(), 'ERROR');
				return false;
			}
		}
		//обновление контакта
		else {
			log_event('Contact found. ID: '.$contact['id']);
			try {
				$postData['request']['contacts']['update'][] = array(
					'linked_leads_id'=>array($lead_id),
					'last_modified' => $timestart,
					'id' => $contact['id'],
					'responsible_user_id' => $contact['responsible_user_id'],
				);

				log_event(json_encode($postData), 'DEBUG');
				amo_curl_post('/private/api/v2/json/contacts/set', $postData, $this->AMOCRM_SUBDOMAIN);
				log_event("Contact has been updated");
				unset($postData);
			}
			catch(Exception $E)	{
				log_event('Failed to update contact', 'ERROR');
				log_event('Error '.$E->getCode().': '.$E->getMessage(), 'ERROR');
				return false;
			}
		}

		//создание задачи
		try {
			$postData['request']['tasks']['add'][] = array(
				'element_id' => $lead_id,
    		"element_type" => 2,
    		"task_type" => 4,
    		"text" => "Заявка с gf-a.ru" ,
    		"complete_till" => $timestart + $this->task_complete_till,
			);

			log_event(json_encode($postData), 'DEBUG');
			amo_curl_post('/private/api/v2/json/tasks/set', $postData, $this->AMOCRM_SUBDOMAIN);
			log_event("Task has been created");
			unset($postData);
		}
		catch(Exception $E)	{
			log_event('Failed to update contact', 'ERROR');
			log_event('Error '.$E->getCode().': '.$E->getMessage(), 'ERROR');
			return false;
		}

		//добавление примечания
		if(isset($data['subject'])) {
			try {
				$postData['request']['notes']['add'][] = array(
					'element_id' => $lead_id,
	    		"element_type" => 2,
	    		"note_type" => 4,
	    		"text" => $data['subject'] ."\r\n". $data['body'],
				);

				log_event(json_encode($postData), 'DEBUG');
				amo_curl_post('/private/api/v2/json/notes/set', $postData, $this->AMOCRM_SUBDOMAIN);
				log_event("Note has been created");
				unset($postData);
			}
			catch(Exception $E)	{
				log_event('Failed to add note', 'ERROR');
				log_event('Error '.$E->getCode().': '.$E->getMessage(), 'ERROR');
				return false;
			}
		}

		log_event('End');
		return true;
	}

	/**
	 * Функция поиска контакта по номеру телефона и почте
	 * @param $phone string номер телефона.
	 * @param $email string почтовый адрес.
	 * @return array найденный контакт либо bolean false если нет контакта
	 */
  private function amocrm_check($phone = null, $email = null) {
	  log_event('Amocrm_check: start', 'DEBUG');
	  if (!empty($email)) {
	    //тупой яндекс с их двойными адресами
	    $yandex = null;
	    if (stripos($email, '@ya.ru')) {
	      $yandex = '@ya.ru';
	    }
	    if (stripos($email, '@yandex.ru')) {
	      $yandex = '@yandex.ru';
	    }

	    $args = array('query' => $email);
	    $responce = amo_curl_get('/private/api/v2/json/contacts/list', $args, null, $this->AMOCRM_SUBDOMAIN);
	    if(isset($responce)) {
	      foreach($responce['contacts'] as $contact) {
	        if(isset($contact['custom_fields'])) {
	          foreach ($contact['custom_fields'] as $custom_field) {
	            if ($custom_field['code'] == 'EMAIL') {
	              foreach ($custom_field['values'] as $value) {
	                if ($value['value'] == $email) {
	                  log_event('Amocrm_check: e-mail found. Contact ID: '.$contact['id'], 'DEBUG');
	                  return $contact;
	                }
	              }
	            }
	          }
	        }
	      }
	    }
	    if (!empty($yandex)) {
	      //если яндекс - меняем адрес на альтернативный и снова ищем.
	      if ($yandex == '@ya.ru') {
	        $email = str_replace('@ya.ru', '@yandex.ru', $email);
	      }
	      else {
	        $email = str_replace('@yandex.ru', '@ya.ru', $email);
	      }
	      $args = array('query' => $email);
	      $responce = amo_curl_get('/private/api/v2/json/contacts/list', $args, null, $this->AMOCRM_SUBDOMAIN);
	      if(isset($responce)) {
	        foreach($responce['contacts'] as $contact) {
	          if(isset($contact['custom_fields'])) {
	            foreach ($contact['custom_fields'] as $custom_field) {
	              if ($custom_field['code'] == 'EMAIL') {
	                foreach ($custom_field['values'] as $value) {
	                  if ($value['value'] == $email) {
	                    log_event('Amocrm_check: e-mail found. Contact ID: '.$contact['id'], 'DEBUG');
	                    return $contact;
	                  }
	                }
	              }
	            }
	          }
	        }
	      }
	    }
	  }
	  if (!empty($phone)) {
	    $cut = false;
	    if (substr($phone, 0, 1) == '8' && strlen($phone) == 11) {
	      $phone = substr($phone, 1);
	      $cut = true;
	    }
	    if (substr($phone, 0, 2) == '+7') {
	      $phone = substr($phone, 2);
	      $cut = true;
	    }
	    $args = array('query' => $phone);
	    $responce = amo_curl_get('/private/api/v2/json/contacts/list', $args, null, $this->AMOCRM_SUBDOMAIN);
	    if(isset($responce)) {
	      foreach($responce['contacts'] as $contact) {
	        if(isset($contact['custom_fields'])) {
	          foreach ($contact['custom_fields'] as $custom_field) {
	            if ($custom_field['code'] == 'PHONE') {
	              foreach ($custom_field['values'] as $value) {
	                if ($cut == true) {
	                  if ($value['value'] == '8'.$phone || $value['value'] == '+7'.$phone) {
	                    log_event('Amocrm_check: phone found. Contact ID: '.$contact['id'], 'DEBUG');
	                    return $contact;
	                  }
	                }
	                else {
	                  if ($value['value'] == $phone) {
	                    log_event('Amocrm_check: phone found. Contact ID: '.$contact['id'], 'DEBUG');
	                    return $contact;
	                  }
	                }
	              }
	            }
	          }
	        }
	      }
	    }
	  }
	  log_event('Amocrm_check: nothing was found.', 'DEBUG');
	  return false;
	}
}