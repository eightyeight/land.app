<?php

class SiteController extends CController
{
    public function actionIndex()
    {

        $model = new TargetForm();

    	$this->render('index', array('model' => $model));
		      
    }

    public  function actionError(Exeption $e){

    	echo 'Error msg:';
    	echo $e->getMessage();

    }

    public function actionSend(){

            if(Yii::app()->request->isAjaxRequest){

                $model = new TargetForm();

                $model->attributes= $_POST['TargetForm'];

                $searchEngine = null;

                $searchQuery = null;

                if($model->validate()){

                    try{

                        $amocrm = Yii::app()->amocrm;

                        $amocrm->addFormData($model);

                    }
                    catch(Exeption $e){

                        echo "amocrm not sync!";

                    }

                    try{

                        if($this->sendHtmlMail($model->name, $model->phone, $model->email, $searchEngine, $searchQuery))
                            echo CJSON::encode(array(
                                'status'=>'success'
                            ));

                        Yii::app()->end();

                    }

                    catch(Swift_TransportException $e){

                        echo 'Error msg:';

                        echo $e->getMessage();

                    }
                }
                else{

                    $error = CActiveForm::validate($model);

                    if($error!='[]')
                        echo $error;

                    Yii::app()->end();
                }
            }
    }

	public function sendHtmlMail($name, $phone, $email, $searchEngine, $searchQuery) {

		Yii::import('vendor.swiftmailer.swiftmailer.lib.swift_required', true);

		$subject = Yii::app()->request->hostinfo .': Получен новый лид.';

		$body = '<html>' .
				' <head></head>' .
				'<body>'.
				'<p>
				Здравствуйте
				</p>

				<p>	
				Получен новый лид с целевой страницы: '. Yii::app()->request->hostinfo .'
				</p>

				<p>
				Данные лида:
				</p>
                <p>
                Cайт: "'.Yii::app()->request->hostinfo.'"	
                </p>
                <p>
                Поисковая ситема: "'.$searchEngine.'"	
                </p>
                <p>
                Запрос: "'.$searchQuery.'"	
                </p>

				<p>
				Имя: "'. $name .'" 
				</p>

				<p>
				Телефон: "'. $phone .'"
                </p>

				<p>
				Почта: "' . $email . '"
				</p>

				<p>
				--
				Это автоматическое сообщение, отвечать на него не нужно. 
				</p>
				'.	
				' </body>' .
				'</html>';
	 
	    $message = Swift_Message::newInstance()
	      ->setSubject($subject)
	      ->setFrom('noreply@gf-a.ru')
	      ->setTo('bodrov@gf-a.ru')
	      ->setBody($body, 'text/html');


	 
	    $transport = Swift_SmtpTransport::newInstance();
	 
	    $mailer = Swift_Mailer::newInstance($transport);

	    return $mailer->send($message);
	}


	public function actionThanks(){

		$this->render('thanks');

	}

    protected function performAjaxValidation($model) {
        if(isset($_POST['ajax'])) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
