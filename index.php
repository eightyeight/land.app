<?php

$yii=dirname(__FILE__).'/../yii/framework/yii.php';

$config=dirname(__FILE__).'/app/config/main.php';

// Не забываем отключать дебаг на продакшене.

defined('YII_DEBUG') or define('YII_DEBUG', true);

defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

// @ToDo Раскомментировать после  установки зависимотей через composer и генерации автозагрузчика.

$loader = dirname(__FILE__) . '/vendor/autoload.php';

require_once($loader);

require_once($yii);

Yii::createWebApplication($config)->run();

