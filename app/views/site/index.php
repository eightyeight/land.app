<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Оклейка автомобилей пленкой</title>

    <link href="css/fonts.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
<div class="wrapper headerwrap">
    <div class="wrap-in">
        <div id ="header" class="row">
            <div id="logo" class="column c2">
                <img src="img/logo-white.png" />
            </div>
            <div id="title" class="column c6">
                <h1>Оклейка автомобилей пленкой</h1>
            </div>
            <div id="callus" class="column c2">
                <span>Есть вопрос? <br/> Звони - поможем!</span>
            </div>
            <div id="phone" class="column c2">
                <span>8(495)25-19-22</span>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</div>

<div class="wrapper subheaderwrap">
    <div class="wrapper-middle">
        <div class="wrap-in">
            <div id="subheader" class="row">
                <div id="getcoupon" class="column c3 l9">
                    <?php $form = $this->beginWidget('CActiveForm',
                        array(
                            'id' => 'target-form',
                            'enableAjaxValidation'=>true,
                            'enableClientValidation'=>true,
                            'action'=> CHtml::normalizeUrl(array('site/send')),
                            'clientOptions'=>array(
                                'validateOnSubmit'=>true,
                                'validateOnChange'=>false,
                            ),
                        )
                    ) ?>
                        <ul>
                            <li id="title">
                                <span>Получи купон на скидку</span>
                            </li>
                            <li>
                                <?php echo $form->textField($model, 'name', array('placeholder' => 'Ваше Имя'))?>
                                <?php echo $form->error($model,'name'); ?>
                            </li>
                            <li>
                                <?php echo $form->textField($model, 'phone', array('placeholder' => 'Ваш Телефон'))?>
                                <?php echo $form->error($model,'phone'); ?>
                            </li>
                            <li>
                                <?php echo $form->textField($model, 'email', array('placeholder' => 'Ваше Email'))?>
                                <?php echo $form->error($model,'email'); ?>
                            </li>
                            <li>
                                <?php echo CHtml::ajaxSubmitButton('Написать', $this->createUrl('site/send'),
                                    array(
                                        'type'=>'POST',
                                        'success'=>'function(data){
                                            var data = jQuery.parseJSON(data);

                                            if(data.status=="success"){

                                                var url = "http://auto.gf-a.ru/index.php?r=site/thanks";

                                                $(location).attr("href",url);
                                            }
                                            else{
                                                    jQuery.each(data, function(key, value) {
                                                        jQuery("#"+key+"_em_").show().html(value.toString());
                                                    });
                                            }
                                        }'
                                    ),
                                    array(
                                        'class' => 'submit-button',
                                    )
                                );?>
                            </li>
                        </ul>
                    <?php $this->endWidget();?>
                </div>
                <div class="clr"></div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper serviceswrap">
    <div class="wrap-in">
        <div id="services" class="row">
            <div id="title" class="row">
                <h3>Наши услуги</h3>
            </div>
            <div id="services-in" class="row">
                <div id="services-in-in" class="column c9 l2">
                    <ul>
                        <li id="brand" class="column c2">
                            <div id="img"><img src="img/service-brand.png" /></div>
                            <div id="title"><span>Брендирование</span></div>
                        </li>
                        <li class="column c2">
                            <div id="img"><img src="img/service-design.png" /></div>
                            <div id="title"><span>Дизайн</span></div>
                        </li>
                        <li class="column c2">
                            <div id="img"><img src="img/service-vinyl.png" /></div>
                            <div id="title"><span>Винилография</span></div>
                        </li>
                        <div class="clr"></div>
                    </ul>
                </div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</div>

<div class="wrapper choosewrap">
    <div class="wrap-in">
        <div id="choose" class="row">
            <div id="title" class="row">
                <h3>Наши клиенты выбирают</h3>
            </div>
            <div id="choose-in" class="row">
                <div id="choose-in-in" class="column c11 l1">
                    <ul>
                        <li id="premium">
                            <div id="round"><span>Premium</span></div>
                            <div id="oracal"><span>Oracal 951</span></div>
                        </li>
                        <li id="metal">
                            <div id="round"><span>Металл</span></div>
                            <div id="oracal">
										<span>Oracal 383<br/>
											  Oracal 351<br/>
											  Oracal 352
										</span>
                            </div>
                        </li>
                        <li id="factur">
                            <div id="round"><span>Фактурные</span></div>
                            <div id="oracal"><span>Oracal 975</span></div>
                        </li>
                        <li id="care">
                            <div id="round"><span>Защитная</span></div>
                            <div id="oracal"><span>Oracal 970</span></div>
                        </li>
                        <div class="clr"></div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper whyuswrap">
    <div class="wrapper-middle">
        <div class="wrap-in">
            <div id="whyus" class="row">
                <div id="title" class="row">
                    <h3>Почему мы?</h3>
                </div>
                <div id="whyus-in" class="row">
                    <div id="whyus-in-in" class="column c11 l1">
                        <ul>
                            <li id="skin">
                                <div id="img"><img src="img/whyus-skin.png" /></div>
                                <div id="title"><span>Высококачественная пленка</span></div>
                            </li>
                            <li id="professionals">
                                <div id="img"><img src="img/whyus-professionals.png" /></div>
                                <div id="title"><span>Профессиональные исполнители</span></div>
                            </li>
                            <li id="lowcost">
                                <div id="img"><img src="img/whyus-lowcost.png" /></div>
                                <div id="title"><span>Цены ниже рынка</span></div>
                            </li>
                            <div class="clr"></div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper howeconomywrap">
    <div class="wrapper-middle">
        <div class="wrap-in">
            <div id="howeconomy" class="row">
                <div id="howeconomy-in" class="row">
                    <div id="howeconomy-in-in" class="column c12">
                        <div id="title" class="row">
                            <h3>Как сэкономить на ремонте авто <span>до 40 тысяч</span> за полгода?</h3>
                        </div>
                        <div id="rounds-top" class="row">
                            <div id="save">
                                <div id="title"><span>Сохранить товарный вид</span></div>
                                <div id="img"><img src="img/howeconomy-save.png" /></div>
                            </div>
                            <div id="clean">
                                <div id="img"><img src="img/howeconomy-clean.png" /></div>
                                <div id="title"><span>Облегчить мытье машины</span></div>
                            </div>
                            <div class="clr"></div>
                        </div>
                        <div id="rounds-bottom" class="row">
                            <div id="protect">
                                <div id="title"><span>Защитить машину от сколов</span></div>
                                <div id="img"><img src="img/howeconomy-protect.png" /></div>
                            </div>
                            <div id="car">
                                <div id="img"><img src="img/howeconomy-car.png" /></div>
                            </div>
                            <div id="change">
                                <div id="img"><img src="img/howeconomy-change.png" /></div>
                                <div id="title"><span>Изменить декор машины</span></div>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper howitworkswrap">
    <div class="wrapper" id="wave-border-top"></div>
    <div class="wrapper" id="howitworkswrap-in">
        <div class="wrap-in">
            <div id="howitworks" class="row">
                <div id="howitworks-in" class="column c12">
                    <div id="title" class="row">
                        <h3>Как мы работаем</h3>
                    </div>
                    <div id="howitworks-in-in">
                        <div id="request" class="row">
                            <div id="title">Вы оставляете заявку</div>
                            <div id="img"><img src="img/howitworks-request.png" /></div>
                        </div>
                        <div id="manager" class="row">
                            <div id="img"><img src="img/howitworks-manager.png" /></div>
                            <div id="title">Наш менеджер связывается с вами и рассказывает об услугах, которые вам интересны</div>
                        </div>
                        <div id="time" class="row">
                            <div id="title">Вы выбираете удобное для вас время и записываетесь на замер</div>
                            <div id="img"><img src="img/howitworks-time.png" /></div>
                        </div>
                        <div id="design" class="row">
                            <div id="img"><img src="img/howitworks-design.png" /></div>
                            <div id="title">Мы разрабатываем дизайн или адаптируем макет</div>
                        </div>
                        <div id="taping" class="row">
                            <div id="title">Делаем оклейку</div>
                            <div id="img"><img src="img/howitworks-taping.png" /></div>
                        </div>
                        <div id="done" class="row">
                            <div id="img"><img src="img/howitworks-done.png" /></div>
                            <div id="title">Вы забираете свой обновленный автомобиль</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clr"></div>
<div class="wrapper" id="wave-border-bottom"></div>

<div class="wrapper portfoliowrap">
    <div class="wrap-in">
        <div id="portfolio" class="row">
            <div id="title" class="row">
                <h3>Портфолио</h3>
            </div>
            <div id="portfolio-slider" class="row">
                <div id="arrow-left" class="column c1"><img src="img/portfolio-arrow-left.png" /></div>
                <div id="slider" class="column c10">
                    <ul>
                        <li class="first"><img src="img/slider-1.png" /></li>
                        <li><img src="img/slider-2.png" /></li>
                        <li class="last"><img src="img/slider-3.png" /></li>
                    </ul>
                    <div class="clr"></div>
                </div>
                <div id="arrow-right" class="column c1"><img src="img/portfolio-arrow-right.png" /></div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper orderwrap">
    <div class="wrapper-middle">
        <div class="wrap-in">
            <div id="order" class="row">
                <div id="title" class="row">
                    <h3>Закажите оклейку авто сейчас</h3>
                </div>
                <div id="subtitle" class="row">
                    <span>и получите скидку 5%</span>
                </div>
                <div id="order-in" class="row">
                    <?php $form = $this->beginWidget('CActiveForm',
                        array(
                            'id' => 'target-form',
                            'enableAjaxValidation'=>true,
                            'enableClientValidation'=>true,
                            'action'=> CHtml::normalizeUrl(array('site/send')),
                            'clientOptions'=>array(
                                'validateOnSubmit'=>true,
                                'validateOnChange'=>false,
                            ),
                        )
                    ) ?>
                        <ul>
                            <li>
                                <?php echo $form->textField($model, 'name', array('placeholder' => 'Ваше Имя'))?>
                                <?php echo $form->error($model,'name', array('id' => CHtml::activeId($model,'name') . '_emb_', 'class' => 'errorMessageBottom')); ?>
                            </li>
                            <li>
                                <?php echo $form->textField($model, 'phone', array('placeholder' => 'Ваш Телефон'))?>
                                <?php echo $form->error($model,'phone', array('id' => CHtml::activeId($model,'phone') . '_emb_', 'class' => 'errorMessageBottom')); ?>
                            </li>
                            <li>
                                <?php echo $form->textField($model, 'email', array('placeholder' => 'Ваше Email'))?>
                                <?php echo $form->error($model,'email', array('id' => CHtml::activeId($model,'email') . '_emb_', 'class' => 'errorMessageBottom')); ?>
                            </li>
                            <li>
                                <?php echo CHtml::ajaxSubmitButton('Заказать', $this->createUrl('site/send'),
                                    array(
                                        'type'=>'POST',
                                        'success'=>'function(data){



                                            var data = jQuery.parseJSON(data);

                                            if(data.status=="success"){

                                                var url = "http://auto.gf-a.ru/index.php?r=site/thanks";

                                                $(location).attr("href",url);
                                            }
                                            else{
                                                    jQuery.each(data, function(key, value) {

                                                        jQuery("#"+key+"_emb_").show().html(value.toString());
                                                    });
                                            }
                                        }'
                                    ),
                                    array(
                                        'class' => 'submit-button',
                                    )
                                );?>
                            </li>
                        </ul>
                    <?php $this->endWidget()?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper footerwrap">
    <div class="wrapper scissors"></div>
    <div class="wrap-in">
        <div id="footer" class="row">
            <div id="footer-in" class="column c12">
                <div id="projects">
                    <ul>
                        <li><span>Наши проекты:</span></li>
                        <li><a href="#">Световые вывески</a></li>
                        <li><a href="#">Широкоформатная печать</a></li>
                        <li><a href="#">Регистрация рекламы</a></li>
                    </ul>
                </div>
                <div id="contacts">
                    <div id="phone">Телефон: +7(495)656-98-63</div>
                    <div id="email">E-mail: zakaz@gf-a.ru</div>
                    <div id="copyright">© 2014 Рекламная группа "Жираф". Все права защищены.</div>
                </div>
                <div class="clr"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>