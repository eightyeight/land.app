<?php

class UtmParser {

	public static $urlParam;

	public static function parseUrl($param){

    	if(Yii::app()->request->getParam($param))
    		self::$urlParam = Yii::app()->request->getParam($param);
    	else 
    		self::$urlParam = 'СВЕТОВЫХ БУКВ';



    	$strEnd = strlen(self::$urlParam) > 25 ? '</br>' : '';

    	echo self::$urlParam . $strEnd;
	}

	public static function getSearchEngine($urlParam){

		return Yii::app()->request->getParam($urlParam);

	}

	public static function getSearchQuery($urlParam){
		
		return Yii::app()->request->getParam($urlParam);

	}

}