<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 30.10.14
 * Time: 15:55
 */

class TargetForm extends CFormModel{

    public $name;

    public $phone;

    public $email;

    public function rules(){
        return array(
            array('name, phone', 'required'),
            array('email,','email'),
            array('name, phone, email', 'safe'),
        );
    }

    public function attributeLabels(){
        return array(
          'name' => 'Ваше Имя',
          'phone' => 'Ваш Телефон',
          'email' => 'Ваш Email',
        );
    }
} 