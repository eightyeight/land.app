<?php

return array(
	'basePath'  =>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'      =>'LP',
    'language'  =>'ru',

    'preload' => array(
        'log',
    ),

    'aliases'=>array(
        'vendor' => realpath(__DIR__ . '/../../vendor'),
    ),

    'import'=>array(
        'application.vendor.swiftmailer.swiftmailer.lib.*',
        'application.helpers.*',
        'application.form.*',
    ), 

	'components'=>array(
        'clientScript' => array(
            'scriptMap' => array(
                'jquery.js' => '/js/jquery.js',
                'jquery.yiiactiveform.js' => '/js/jquery.yiiactiveform.js'
            )
        ),
        'assetManager'=>array(
            'basePath'=> realpath(__DIR__. '/../assets'),
        ),
		'errorHandler'=>array(		
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

        'amocrm'=>array(
            'class'=>'application.helpers.amocrm.amocrm',
        ),
	),

	'params'=>array(
		'adminEmail'=>'info@gf-a.ru',
        'sendEmail' =>'zakaz@gf-a.ru',
	),
);